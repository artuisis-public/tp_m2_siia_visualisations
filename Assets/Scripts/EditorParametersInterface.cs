using UnityEngine;

public class EditorParametersInterface : MonoBehaviour
{
    [Header("Swarm behaviour")]
    [SerializeField]
    private BehaviourManager.AgentBehaviour agentBehaviour; //Define the current behaviour of the agents

    [Header("Swarm movement")]
    [SerializeField]
    private MovementManager.AgentMovement agentMovement; //Defines how the agent moves


    private float mapSizeX = 7.0f;
    private float mapSizeZ = 7.0f;

    [Header("Field of view size")]
    [SerializeField]
    [Range(0.0f, 5.0f)]
    [Tooltip("This is the size of the radius.")]
    private float fieldOfViewSize = 1.0f;
    [SerializeField]
    [Range(0, 360)]
    [Tooltip("This is the size of blind spot of the agent (in degrees)")]
    private float blindSpotSize = 30;

    [Header("Intensity parameters")]
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float maxSpeed = 0.1f;
    [SerializeField]
    [Range(0.0f, 5.0f)]
    private float randomMovementIntensity = 0.0f;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float frictionIntensity = 0.1f;
    [SerializeField]
    [Range(0.0f, 20.0f)]
    private float avoidCollisionWithNeighboursIntensity = 20.0f;


    [Header("Reynolds model parameters")]
    [SerializeField]
    [Range(0.0f, 5.0f)]
    private float cohesionIntensity = 1.0f;
    [SerializeField]
    [Range(0.0f, 5.0f)]
    private float alignmentIntensity = 1.0f;
    [SerializeField]
    [Range(0.0f, 5.0f)]
    private float separationIntensity = 1.0f;


    public SwarmParameters GetParameters()
    {
        SwarmParameters parameters = new SwarmParameters(agentBehaviour,
                                                        agentMovement,
                                                        mapSizeX,
                                                        mapSizeZ,
                                                        fieldOfViewSize,
                                                        blindSpotSize,
                                                        maxSpeed,
                                                        randomMovementIntensity,
                                                        frictionIntensity,
                                                        avoidCollisionWithNeighboursIntensity,
                                                        cohesionIntensity,
                                                        alignmentIntensity,
                                                        separationIntensity);
        return parameters;
    }

    public void SetParameters(SwarmParameters parameters)
    {
        this.agentBehaviour = parameters.GetAgentBehaviour();
        this.agentMovement = parameters.GetAgentMovement();
        this.mapSizeX = parameters.GetMapSizeX();
        this.mapSizeZ = parameters.GetMapSizeZ();
        this.fieldOfViewSize = parameters.GetFieldOfViewSize();
        this.blindSpotSize = parameters.GetBlindSpotSize();
        this.maxSpeed = parameters.GetMaxSpeed();
        this.randomMovementIntensity = parameters.GetRandomMovementIntensity();
        this.frictionIntensity = parameters.GetFrictionIntensity();
        this.avoidCollisionWithNeighboursIntensity = parameters.GetAvoidCollisionWithNeighboursIntensity();
        this.cohesionIntensity = parameters.GetCohesionIntensity();
        this.alignmentIntensity = parameters.GetAlignmentIntensity();
        this.separationIntensity = parameters.GetSeparationIntensity();
    }
}
