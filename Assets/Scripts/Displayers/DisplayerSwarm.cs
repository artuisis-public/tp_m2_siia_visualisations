using System.Collections.Generic;
using UnityEngine;
using System;


public class DisplayerSwarm : Displayer
{

    #region Serialized fields
    [SerializeField]
    private GameObject actorPrefab;
    #endregion

    #region Private fields

    private List<GameObject> actors = new List<GameObject>();

    private Vector3 spatialOrigin;
    #endregion

    #region Methods - Monobehaviour callbacks
    // Start is called before the first frame update
    void Start()
    {
        this.spatialOrigin = Vector3.zero;
    }
    #endregion

    #region Methods - Displayer override
    public override void DisplayVisual(SwarmData swarmData)
    {
        DisplaySimple(swarmData);
    }


    public override void ClearVisual()
    {
        ClearActors();
    }
    #endregion

    #region Methods - Swarm display
    /// <summary>
    /// Displays the <see cref="SwarmData"/> set in parameter,, 
    /// by displaying the position of saved agents in the clip using actors.
    /// It display the frame in the simpliest way possible, meaning that all actor are the same <see cref="UnityEngine.Color"/>.
    /// </summary>
    /// /// <param name="frame"> The <see cref="LogClipFrame"/> value correspond to the frame which must be displayed.</param>
    public void DisplaySimple(SwarmData swarmData)
    {
        int numberOfAgents = swarmData.GetAgentsData().Count;

        AdjustActorNumber(numberOfAgents);


        //Update actors position
        for (int i = 0; i < numberOfAgents; i++)
        {
            AgentData a = swarmData.GetAgentsData()[i];

            //Update actor position and direction
            UpdateActorPositionAndDirection(i, a.GetPosition(), a.GetDirection());

            //Update actor color
            Renderer actorRenderer = actors[i].GetComponent<Renderer>();
            if(actorRenderer != null)
                actorRenderer.material.color = Color.black;
        }
    }
    #endregion

    #region Methods - Actors management
    /// <summary>
    /// This method check if there is the right amount of actors (<see cref="GameObject"/>) to simulate each agent of a clip.
    /// If there is more, it deletes the surplus actors. 
    /// Is there is less, it create new <see cref="GameObject"/> to fit the right amount of agents.
    /// </summary>
    /// <param name="numberOfAgents"> A <see cref="int"/> value that represent the right amount of actor needed.</param>
    public void AdjustActorNumber(int numberOfAgents)
    {
        int numberOfActors = actors.Count;

        //Create missing actors
        if (numberOfActors < numberOfAgents)
        {
            for (int i = 0; i < (numberOfAgents - numberOfActors); i++)
            {
                GameObject newAgent = GameObject.Instantiate(actorPrefab);
                newAgent.transform.localPosition = Vector3.zero;
                newAgent.transform.localRotation = Quaternion.Euler(0.0f, UnityEngine.Random.Range(0.0f, 359.0f), 0.0f);
                newAgent.transform.parent = this.transform;
                actors.Add(newAgent);
            }
        }

        //Destroy surplus actors
        if (numberOfActors > numberOfAgents)
        {
            for (int i = numberOfAgents; i < numberOfActors; i++)
            {
                GameObject.Destroy(actors[i].gameObject);
            }
            actors.RemoveRange(numberOfAgents, numberOfActors - numberOfAgents);
        }
    }

    /// <summary>
    /// Remove all actors and destroy their gameObject.
    /// </summary>
    private void ClearActors()
    {
        foreach (GameObject a in actors)
        {
            GameObject.Destroy(a.gameObject);
        }
        actors.Clear();
    }

    private void UpdateActorDirection(int actorId, Vector3 direction)
    {
        float agentDirection_YAxis = 180 - (Mathf.Acos(direction.normalized.x) * 180.0f / Mathf.PI);
        if (direction.z < 0.0f) agentDirection_YAxis = agentDirection_YAxis * -1;
        actors[actorId].transform.localRotation = Quaternion.Euler(0.0f, agentDirection_YAxis, 0.0f);
    }

    private void UpdateActorPosition(int actorId, Vector3 position)
    {
        actors[actorId].transform.localPosition = position + spatialOrigin;
    }

    private void UpdateActorPositionAndDirection(int actorId, Vector3 position, Vector3 direction)
    {
        UpdateActorPosition(actorId, position);
        UpdateActorDirection(actorId, direction);
    }
    #endregion

    #region Methods - Setter
    public void SetSpatialOrigin(Vector3 origin)
    {
        this.spatialOrigin = origin;
    }

    public void setActorPrefab(GameObject prefab)
    {
        this.actorPrefab = prefab;
        ClearActors();
    }
    #endregion
}
